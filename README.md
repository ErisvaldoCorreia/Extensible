# Extensible
![Logo Extensible library](extensible-logo.png)

![version](https://img.shields.io/badge/version-1.0.0-green.svg)
![node required](https://img.shields.io/badge/node-%3E%3D7.9.0-red.svg)

> The use of a tool, however simple it may be,  
> makes our work always a bit less complicated.

Extensible is a simple library for starting your web projects.

###### Author:
Erisvaldo Correia
